const express = require('express');
const app = express();
const router = require('./src/router');
require('dotenv').config();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(router);

const port = parseInt(process.env.SERVER_PORT);
app.listen(port, () => console.log(`Listening on port ${port}`));