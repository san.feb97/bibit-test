const db = require('../connection');
const axios = require('axios');

const urlLink = process.env.API_URL;
const urlKey = process.env.API_KEY;

module.exports = {

    searchMovie: async (req, res) => {
        try {
            const dataObj = req.query;
            if (dataObj.s == undefined) { 
                res.status(400).json({ status: false, msg: 'missing search query' }); 
            } else { 
                const parameters = [];
                for (let key in dataObj) {
                    if (key != 's') {
                        parameters.push(`${key}=${dataObj[key]}`);
                    }
                }

                const getResult = await axios.get(`${urlLink}/?apikey=${urlKey}&s=${dataObj.s ? dataObj.s : ''}&${parameters.join('&')}`);

                db.query(`INSERT INTO movie_log_table (endpoint, parameter) VALUES ('${req.route.path}', 's=${dataObj.s}&${parameters.join('&')}'); `,function (err, result, fields) {
                    if (err) {
                        console.log(err);
                        res.status(500).json({ status: false, msg: 'error insert log', data: getResult.data });
                    } else {
                        if (result.insertId) {
                            res.status(200).json({ status: true, msg: 'success insert log', data: getResult.data });
                        } else {
                            res.status(200).json({ status: false, msg: 'failed insert log', data: getResult.data });
                        }
                    }
                });
            }
        } catch (error) {
            res.status(500).json({ status: false, msg: 'error get from api', data: error.response.data });
        }
    },

    detailMovie: async (req, res) => {
        try {
            const dataObj = req.query;
            const parameters = [];
            for (let key in dataObj) {
                if (key == 's') {
                    delete key;
                } else {
                    parameters.push(`${key}=${dataObj[key]}`);
                }
            }

            const getResult = await axios.get(`${urlLink}/?apikey=${urlKey}&${parameters.join('&')}`);

            db.query(`INSERT INTO movie_log_table (endpoint, parameter) VALUES ('${req.route.path}', '${parameters.join('&')}'); `,function (err, result, fields) {
                if (err) {
                    console.log(err);
                    res.status(500).json({ status: false, msg: 'error insert log', data: getResult.data });
                } else {
                    if (result.insertId) {
                        res.status(200).json({ status: true, msg: 'success insert log', data: getResult.data });
                    } else {
                        res.status(200).json({ status: false, msg: 'failed insert log', data: getResult.data });
                    }
                }
            });
        } catch (error) {
            res.status(500).json({ status: false, msg: 'error get from api', data: error.response.data });
        }
    },

}