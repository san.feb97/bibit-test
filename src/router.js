const express = require('express');
const router = express.Router();
const controller = require('./controller');

router.get(`/`, (req, res) => {
    res.status(200).json('Hello Bibit');
});

router.get(`/search`, (req, res) => {
    controller.searchMovie(req, res);
});

router.get(`/detail`, (req, res) => {
    controller.detailMovie(req, res);
});

module.exports = router;
