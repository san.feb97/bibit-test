# 1. Simple Database Querying
1. menuju link [phpMyAdmin](https://www.phpmyadmin.co/index.php), lalu login dengan memasukkan:
- server: `sql6.freesqldatabase.com`
- username: `sql6445260`
- password: `JAYrXR3T8J`

2. pilih database `sql6445260` pada menu sidebar.

3. untuk melihat isi tabel `USER`, klik menu `SQL` pada header menu, lalu isi query berikut:
```sql
SELECT * FROM USER;
```

4. untuk mendapatkan hasil yang diinginkan, isi query berikut:
```sql
SELECT
    e.UserName UserName,
    m.UserName ParentUserName
FROM
    USER e
LEFT JOIN USER m ON m.ID = e.Parent
ORDER BY e.ID;
```

# 2. Small ExpressJS Server to Search Movies

1. jalankan perintah `git clone https://gitlab.com/san.feb97/bibit-test.git` untuk clone repository.

2. masuk ke direktori project dengan perintah `cd bibit-test\`

3. melakukan penginstallan beberapa node package manager dengan menjalan kan perintah `npm install`, berikut package yang akan diinstall:
- [ExpressJS](https://www.npmjs.com/package/express) digunakan sebagai framework utama dalam project.
- [nodemon](https://www.npmjs.com/package/nodemon) supaya tidak perlu mematikan server saat terdapat perubahan pada kode.
- [mysql](https://www.npmjs.com/package/mysql) digunakan untuk menghubungkan server dengan basis data.
- [dotenv](https://www.npmjs.com/package/dotenv) untuk menyimpan `environment variables`.
- [axios](https://www.npmjs.com/package/axios) untuk menjalan API dari [http://www.omdbapi.com/](#http://www.omdbapi.com/).

3. membuat file `.env` sebagai penyimpan `environment variables` yang berisikan: 
```
SERVER_PORT=3000
DB_HOST=sql6.freesqldatabase.com
DB_USER=sql6445260
DB_PW=JAYrXR3T8J
DB_NAME=sql6445260
API_KEY=<api key>
API_URL=http://www.omdbapi.com
```
buat api key [disini](http://www.omdbapi.com/apikey.aspx).

5. jalankan perintah `npx nodemon .` pada terminal. Jika terdapat log `Listening to port ...` dan `connected to database as id ...` menandakan server sudah dapat berjalan dan sudah terkoneksi dengan basis data.

6. buka browser atau [postman](https://www.postman.com/)

7. coba menjalankan API dengan memasukan dengan endpoint `GET /search` dan `GET /detail` dengan lemparan query yang sesuai dengan [http://www.omdbapi.com/](http://www.omdbapi.com/). contoh: `http://localhost:3000/detail?t=Jurassic&y=1993` atau `http://localhost:3000/search?s=Jurassic&page=3`

8. untuk melihat data log yang ada pada database, dapat dilakukan dengan cara menuju link [phpMyAdmin](https://www.phpmyadmin.co/index.php), lalu login dengan memasukkan:
- server: `sql6.freesqldatabase.com`
- username: `sql6445260`
- password: `JAYrXR3T8J`

9. pilih database `sql6445260` pada menu sidebar, pilih header menu `SQL` dan jalankan query berikut untuk melihat data pada table `movie_log_table`:
```sql
SELECT * FROM `movie_log_table`
```

# 3. Refactor Code 
1. buka browser lalu tekan `f12`, kemudian menuju menu `Console` pada header menu.

2. copy kode berikut, lalu paste pada `Console` browser dan tekan `enter` untuk melihat hasilnya.
```javascript
function findFirstStringInBracket(str){
    if (typeof str == 'string') {
        let afterBracket = str.indexOf('(') > -1 ? str.slice(str.indexOf('(') + 1, str.length) : '';
        return afterBracket.indexOf(')') > -1 ? afterBracket.slice(0, afterBracket.indexOf(')')) : '';
    }
}
findFirstStringInBracket('hasan(nama depan), nabil(nama belakang)')
```

# 4. Logic Test 
1. buka browser lalu tekan `f12`, kemudian menuju menu `Console` pada header menu.

2. copy kode berikut, lalu paste pada `Console` browser dan tekan `enter` untuk melihat hasilnya.
```javascript
function getAnagramArray(words) {

    var sortedTemplate = getUniqueSortedWordAndTemplate(words);
    var result = getGroupingWord(words, sortedTemplate.uniqueWord, sortedTemplate.templateArray);

    return result.sort(function(a, b) {
        return a.length == b.length ? b[0].length - a[0].length : b.length - a.length 
    });
}

function getUniqueSortedWordAndTemplate(words) {

    var templateArray = [];
    var uniqueWord = [];

    for (var index = 0; index < words.length; index++) {

        var sortedWord = words[index].split('').sort().join('').toLowerCase();

        if (uniqueWord.indexOf(sortedWord) == -1) {
            uniqueWord.push(sortedWord);
            templateArray.push([]);
        }

    }

    return { templateArray: templateArray, uniqueWord: uniqueWord };
}

function getGroupingWord(words, unique, template) {

    for (var index = 0; index < words.length; index++) {

        var getIndex = unique.indexOf(words[index].split('').sort().join('').toLowerCase());

        if (getIndex > -1) {
            template[getIndex].push(words[index]);
        }  
    }

    return template;
}

console.log(getAnagramArray(['kita', 'atik', 'tika', 'aku', 'kia', 'makan', 'kua']))
```